#ifndef GFXTK_BINDINGLAYOUTTYPE_HPP
#define GFXTK_BINDINGLAYOUTTYPE_HPP

namespace gfxtk {
    enum class BindingLayoutType {
        Buffer,
        Sampler,
        Texture,
        StorageTexture,
    };
}

#endif //GFXTK_BINDINGLAYOUTTYPE_HPP
