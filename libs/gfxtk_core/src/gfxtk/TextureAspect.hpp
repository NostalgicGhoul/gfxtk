#ifndef GFXTK_TEXTUREASPECT_HPP
#define GFXTK_TEXTUREASPECT_HPP

namespace gfxtk {
    enum class TextureAspect {
        All,
        StencilOnly,
        DepthOnly,
    };
}

#endif //GFXTK_TEXTUREASPECT_HPP
